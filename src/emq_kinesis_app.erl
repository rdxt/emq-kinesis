-module(emq_kinesis_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
  {ok, Sup} = emq_kinesis_sup:start_link(),
  ok = start_pool(),
  _ = emq_kinesis:load(application:get_all_env()),
  emq_kinesis_config:register(),
  {ok, Sup}.

stop(_State) ->
  emq_kinesis:unload(),
  emq_kinesis_config:unregister().

start_pool() ->
  PoolSize = application:get_env(emq_kinesis, pool_size, 1),
  PoolOpts = [
    {pool_size, PoolSize},
    {worker, emq_kinesis_worker}
  ],
  ok = octopus:start_pool(kinesis, PoolOpts, []).


