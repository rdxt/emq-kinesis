-module(emq_kinesis_worker).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
  terminate/2, code_change/3]).

-export([add_queue/3]).

-record(state, {stream, topics, queue = [], timer}).
%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
  gen_server:start_link(?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([]) ->
  {ok, Key} = application:get_env(emq_kinesis, aws_key),
  {ok, SecretKey} = application:get_env(emq_kinesis, aws_secret_key),
  {ok, Region} = application:get_env(emq_kinesis, region),
  {ok, Service} = application:get_env(emq_kinesis, aws_service),
  {ok, Domain} = application:get_env(emq_kinesis, aws_domain),
  HostNameArgs = [Service, Region, Domain],
  Hostname = string:join(HostNameArgs, "."),
  ok = erlcloud_kinesis:configure(Key, SecretKey, Hostname),
  {ok, Stream} = application:get_env(emq_kinesis, stream),
  Topics = application:get_env(emq_kinesis, topics, "#"),
  StreamBin = list_to_binary(Stream),
  {ok, TRef} = timer:send_after(1000, send),
  {ok, #state{stream = StreamBin, topics = Topics, timer = TRef}}.

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast({add_queue, Topic, Payload}, #state{stream = Stream} = State) ->
  Queue = State#state.queue,
  MatchTopics = State#state.topics,
  NewQueue = case is_topic(MatchTopics, Topic) of
               true -> [{Stream, Topic, Payload} | Queue];
               false -> Queue
             end,
  {noreply, State#state{queue = NewQueue}};
handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(send, #state{queue = [], timer = TRef} = State) ->
  {ok, cancel} = timer:cancel(TRef),
  {ok, NewTRef} = timer:send_after(1000, send),
  {noreply, State#state{queue = [], timer = NewTRef}};
handle_info(send, #state{queue = Queue, timer = TRef} = State) ->
  {ok, cancel} = timer:cancel(TRef),
  NewQueue = lists:foldl(fun({Stream, Topic, Payload}, Acc) ->
    case erlcloud_kinesis:put_record(Stream, Topic, Payload) of
      {ok, [{_, Id} | _]} ->
        lager:info("Put record successful: ~p", [Id]),
        Acc;
      {error, {_, _, _, Json}} ->
        PropList = jsx:decode(Json),
        Msg = proplists:get_value(<<"message">>, PropList),
        lager:info("Put record error: ~p; Desc: ~p", [Stream, Msg]),
        [{Stream, Topic, Payload} | Acc]
    end
                         end, [], Queue),
  {ok, NewTRef} = timer:send_after(1000, send),
  {noreply, State#state{queue = NewQueue, timer = NewTRef}};
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------
is_topic("#", _) -> true;
is_topic([_ | _] = MatchTopics, Topic) ->
  ListT = [begin
             [H | _] = binary:split(T, <<"#">>),
             H
           end || T <- MatchTopics],
  _ = binary:match(Topic, ListT) =/= nomatch;
is_topic(MatchTopics, Topic) when is_binary(MatchTopics) ->
  [MatchT | _] = binary:split(MatchTopics, <<"#">>),
  _ = binary:match(Topic, MatchT) =/= nomatch.

add_queue(Worker, Topic, Payload) ->
  gen_server:cast(Worker, {add_queue, Topic, Payload}).
