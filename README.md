## Overview

Sends mqtt messages to aws kinesis.


## Configure Plugin

File: etc/plugin.config

```erlang
[
  {emq_kinesis, [
    {aws_key, ""},
    {aws_secret_key, ""},
    {aws_service, "kinesis"},
    {aws_domain, "amazonaws.com"},
    %% which region your streams in
    {region, "eu-west-1"},
    %% kinesis stream name
    {stream, "test"},
    %% "#" or <<"topic1/#">> or [<<"flow/#">>, <<"car/#">>]
    {topics, "#"},
    %% sender pool size
    {pool_size, 5}
  ]}
].
```


## Load Plugin

```
./bin/emqttd_ctl plugins load emq_kinesis
```